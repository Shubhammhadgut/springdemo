package com.pran.SpringDemo;

public class Alien 
{
	private int age;
	private Computer com;

	public Alien() {
		System.out.println("In alien class");
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
//		System.out.println("Set age method");
		this.age = age;
	}
	
	public Computer getCom() {
		return com;
	}

	public void setCom(Computer com) {
		this.com = com;
	}
	
	public void code() {
		System.out.println("I am coding.....");
		com.complie();
	}

}
