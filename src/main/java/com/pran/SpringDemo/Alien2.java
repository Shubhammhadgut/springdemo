package com.pran.SpringDemo;

public class Alien2 {
	
	private int age;
	private int mobile;
	private Laptop lapi;
	
	public Alien2(int age,int mobile) {
		this.age = age;
		this.mobile=mobile;
	}

	public int getAge() {
		return age;
	}

//	public void setAge(int age) {
//		this.age = age;
//	}
	
	public int getMobile() {
		return mobile;
	}
	
	public void code()
	{
		System.out.println("Alien 2 coding....");
	}
	

}
